﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace TucsonImap
{
  public  class Tools
    {
        public static string SERVER_NAME = ConfigurationManager.AppSettings["Server_name"];
        public static string DB_NAME = ConfigurationManager.AppSettings["DB_name"];
        public static string FlagTypeId = ConfigurationManager.AppSettings["FlagTypeId"];
        public static string FlagStatusBisId = ConfigurationManager.AppSettings["FlagStatusBisId"];
        public static string FlagStatusId = ConfigurationManager.AppSettings["FlagStatusId"];
        public static string FlagPriority= ConfigurationManager.AppSettings["FlagPriority"];
        public static string InspectionDocumentTypeId= ConfigurationManager.AppSettings["InspectionDocumentTypeId"];
        public static string UserdId = ConfigurationManager.AppSettings["UserdId"];


        public static string connectionString = "Server=" + SERVER_NAME + ";Database=" + DB_NAME + ";Trusted_Connection=True;";


        public static string GetAssetId(string milepoint)
        {
            

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    Console.WriteLine(DateTime.Now.ToString() + " : [SUCCESS] SQL Connection OK To GET AssetId");
                    File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [SUCCESS] SQL Connection OK To GET AssetId");
                    File.AppendAllText("Result.txt", "\r\n");

                }
                catch (Exception e)
                {
                    Console.WriteLine(DateTime.Now.ToString() + " : [ERROR] SQL Connection When  GETTING AssetId (" + e.Message + ")");
                    File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [ERROR] SQL Connection When  GETTING AssetId (" + e.Message + ")");
                    File.AppendAllText("Result.txt", "\r\n");

                }


                SqlCommand sqlCommand = new SqlCommand("GetAssetIdByMilePoint", connection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@valuetocheck", milepoint);

                SqlParameter outputparameter = new SqlParameter();
                outputparameter.ParameterName = "@AssetId";
                outputparameter.SqlDbType = System.Data.SqlDbType.NVarChar;
                outputparameter.Direction = System.Data.ParameterDirection.Output;
                outputparameter.Size = 100;
                sqlCommand.Parameters.Add(outputparameter);

                sqlCommand.ExecuteNonQuery();

                string AssetId = outputparameter.Value.ToString();

                return AssetId;
            }
            }

        public static void InsertFlags (string AssetId,string FlagDate ,string FlagNumber ,string FlagComment, string FilePath, string DocumentName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    Console.WriteLine(DateTime.Now.ToString() + " : [SUCCESS] SQL Connection OK To INSERTING FLAGS");
                    File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [SUCCESS] SQL Connection OK To INSERTING FLAGS");
                    File.AppendAllText("Result.txt", "\r\n");

                }
                catch (Exception e)
                {
                    Console.WriteLine(DateTime.Now.ToString() + " : [ERROR] SQL Connection When INSERTING FLAGS (" + e.Message + ")");
                    File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [ERROR] SQL Connection When  INSERTING FLAGS (" + e.Message + ")");
                    File.AppendAllText("Result.txt", "\r\n");
                    return;
                }

                string queryString = getQuery();


                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@AssetId", AssetId);
                command.Parameters.AddWithValue("@FlagDate", FlagDate);
                command.Parameters.AddWithValue("@FlagNumber", FlagNumber);
                command.Parameters.AddWithValue("@FlagComment", FlagComment);
                command.Parameters.AddWithValue("@FlagTypeId", FlagTypeId);
                command.Parameters.AddWithValue("@UserId", UserdId);
                command.Parameters.AddWithValue("@FilePath", FilePath);
                command.Parameters.AddWithValue("@FlagStatusBisId", FlagStatusBisId);
                command.Parameters.AddWithValue("@FlagStatusId", FlagStatusId);
                command.Parameters.AddWithValue("@FlagPriority", FlagPriority);
                command.Parameters.AddWithValue("@InspectionDocumentTypeId", InspectionDocumentTypeId);
                command.Parameters.AddWithValue("@DocumentName", DocumentName);
                //command.Parameters.AddWithValue("@checkIfSameEmailReceivedSeveral", checkIfSameEmailReceivedSeveral);

                try
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {

                        while (reader.Read())
                        {
                            Console.WriteLine(DateTime.Now.ToString() + " : " + String.Format("{0}", reader["result"]));
                            File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : " + reader["result"].ToString());
                            File.AppendAllText("Result.txt", "\r\n");
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(DateTime.Now.ToString() + " : [ERROR] SQL Query (" + e.Message + ")");
                    File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [ERROR] SQL Query (" + e.Message + ")");
                    File.AppendAllText("Result.txt", "\r\n");
                }




            }
            }

        public static string getQuery()
        {
            return @"   
				declare @FlagId uniqueidentifier 
declare @FlagDocumentId uniqueidentifier 
set @FlagId=(select newid() )
set @FlagDocumentId=(select newid() )


insert into [Flag]
      ([Id]
      ,[AssetId]
      ,[FlagTypeId]
      ,[Number]
      ,[FlagStatusId]
      ,[Comments]
      ,[FlagDate]
      ,[AuthorId]
      ,[LastModified]
      ,[UserId]
      ,[PriorityId]
      ,[StatusBisId]
      ,[ExcludeFromReport])
	  values
	  (@FlagId,
	  @AssetId,
	  @FlagTypeId,
	  @FlagNumber,
	  @FlagStatusId,
	  @FlagComment,
	  @FlagDate,
	  @UserId,
	  getdate(),
	  @UserId,
	  @FlagPriority,
	  @FlagStatusBisId,
	  0
	  )


	  INSERT INTO [dbo].[InspectionDocument]
           ([Id]
           ,[Name]
           ,[Comments]
           ,[PathFile]
           ,[LastModified]
           ,[UserId]
           ,[AssetId]
           ,[InspectionDocumentTypeId]
           ,[FileUploadComplete]
           ,[IncludeInReport])
     VALUES
			 (@FlagDocumentId,
			 @DocumentName,
			 @DocumentName,
			 @FilePath,
			 GetDate(),
			 @UserId,
			 @AssetId,
			 @InspectionDocumentTypeId,
			 1,1)



 INSERT INTO [dbo].[InspectionDocument_Flag]
           ([InspectionDocumentId]
           ,[FlagId]
           ,[DateLinked])
     VALUES (@FlagDocumentId,@FlagId,GetDate())
   ";
        }

    }
}
