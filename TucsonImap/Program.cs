﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using S22.Imap;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Configuration;

namespace TucsonImap
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = ConfigurationManager.AppSettings["Imap_Server"];
            var user = ConfigurationManager.AppSettings["Imap_User"];
            var password = ConfigurationManager.AppSettings["Imap_Password"];
            var emailSender = ConfigurationManager.AppSettings["Imap_EmailSender"];
            var attachmentFolder = ConfigurationManager.AppSettings["PDF_Document_Path"];
            var documentFolder = ConfigurationManager.AppSettings["PDF_Document_Folder_AfterAssetId_Path"]; 
             var toBeSearched = ConfigurationManager.AppSettings["ExpressionTobeSearched"]; 
            string pdfattachmentText = null;


            File.AppendAllText("Result.txt", DateTime.Now.ToString() + " === Tucson Emails Console process started ===");
            File.AppendAllText("Result.txt", "\r\n");

            try
            {
                using (ImapClient client = new ImapClient(server, 993, user, password, AuthMethod.Login, true))
                {
                    // Returns a collection of identifiers of all mails matching the specified search criteria.
                    IEnumerable<uint> uids = client.Search(SearchCondition.From(emailSender).And(SearchCondition.Unseen()));

                    // Download mail messages from the default mailbox.
                    IEnumerable<MailMessage> messages = client.GetMessages(uids,
                        (Bodypart part) => {
                        // We're only interested in attachments.
                        if (part.Disposition.Type == ContentDispositionType.Attachment)
                            {
                            // pdf files have a content-type of application/pdf
                            if (part.Type == ContentType.Application &&
                                   part.Subtype.ToLower() == "pdf")
                                {
                                    return true;
                                }
                                else
                                {
                                    // Skip this attachment, it's not a pdf.
                                    return false;
                                }
                            }

                        // Fetch MIME part and include it in the returned MailMessage instance.
                        return true;
                        }
                    );

                    foreach (var message in messages)
                    {
                        var emailSubject = message.Subject.Substring(message.Subject.IndexOf("Wire Break Alert"));
                        string pdfFilePath = null;
                        string AssetIdPlusPdfFileName = null;
                        string pdfattachment;
                        string locationAndDate;
                        string pipeLocationText;
                        string pipeLocation;
                        string flagDate;

                        if (message.Subject.ToUpperInvariant().Contains(toBeSearched)) //Check if the Wire Break at Zone C
                        {
                            locationAndDate = message.Subject.Substring(message.Subject.IndexOf("#") + 2);

                            pipeLocationText = locationAndDate.GetUntilOrEmpty();
                            pipeLocation = pipeLocationText.Replace("+", string.Empty);

                            var assetid = Tools.GetAssetId(pipeLocation); //Get AssetId by using Mile point 

                            if(assetid== "Not Found")
                            {
                                File.AppendAllText("Result.txt", DateTime.Now.ToString() + " ===[ERROR]=== Asset Not Found for PIPE milepoint:"+ pipeLocation + "===");
                                File.AppendAllText("Result.txt", "\r\n");
                            }
                            else
                            { 
                            flagDate = locationAndDate.Substring(locationAndDate.IndexOf(" ") + 1);

                          /*  Console.WriteLine("assetid:" + assetid);
                            Console.WriteLine("PipeLocation:" + pipeLocation);
                            Console.WriteLine("FlagDate:" + flagDate);*/


                            var attachment = message.Attachments.FirstOrDefault(); //Get the first attachment
                            if (!string.IsNullOrWhiteSpace(attachment.Name))
                            {
                                 AssetIdPlusPdfFileName= assetid + '_' + attachment.Name;
                                 pdfFilePath = attachmentFolder + assetid + documentFolder + AssetIdPlusPdfFileName;
                                using (var fileStream = File.Create(pdfFilePath))
                                {
                                    attachment.ContentStream.Seek(0, SeekOrigin.Begin);
                                    attachment.ContentStream.CopyTo(fileStream);
                                }

                                    pdfattachment = ExtractTextFromPdf(pdfFilePath); // convert the pdf to TXT
                                    pdfattachmentText= pdfattachment.Substring(pdfattachment.IndexOf("From:")); 
                                }

                           // Console.WriteLine(pdfattachmentText);

                                Tools.InsertFlags(assetid, flagDate, emailSubject, pdfattachmentText, AssetIdPlusPdfFileName, attachment.Name);

                            }
                        }
                        else
                        {
                            Console.WriteLine("No WireBreak Emails");
                            File.AppendAllText("Result.txt", DateTime.Now.ToString() + " ===No WireBreak Emails Found===");
                            File.AppendAllText("Result.txt", "\r\n");
                        }
                    }
                }

                File.AppendAllText("Result.txt", DateTime.Now.ToString() + " === Tucson Emails Console process completed ===");
                File.AppendAllText("Result.txt", "\r\n");

            }
            catch (Exception e)
            {

                File.AppendAllText("Result.txt", DateTime.Now.ToString() + " : [ERROR] Retriving Emails : " + e.Message);
                File.AppendAllText("Result.txt", "\r\n");
            }
          



        }

       public static string ExtractTextFromPdf(string path)
         {
             using (PdfReader reader = new PdfReader(path))
             {
                 StringBuilder text = new StringBuilder();

                 for (int i = 1; i <= reader.NumberOfPages; i++)
                 {
                     text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                 }

                 return text.ToString();
             }
         } 

    }

    static class Helper
    {
        public static string GetUntilOrEmpty(this string text, string stopAt = " ")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }
    }



}
